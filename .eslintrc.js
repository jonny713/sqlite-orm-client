module.exports = {
  root: true,

  env: {
    node: true
  },

  parserOptions: {
    parser: "babel-eslint"
  },

  rules: {
    "no-debugger": "off"
  },

  extends: ["plugin:vue/recommended", "eslint:recommended", "@vue/prettier"]
};
