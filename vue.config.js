/**
 * Возвращает объект с полями вида:
 * `^/port/tables: объект proxy с переадрессацией на соответствующий порт`
 *
 * @param {number} start Начальный порт
 * @param {number} length кол-во портов
 */
const proxyPathsBuilder = (start, length) => {
    return new Array(length).fill(start).reduce((a, p, i) => a = { [`^/${p + i}/tables`]: {
        target: `http://localhost:${p + i}`,
        pathRewrite: {
          [`^/${p + i}/tables`]: '/tables'
        }
    }, ...a}, {})
}

module.exports = {
  transpileDependencies: ["vuetify"],
  devServer: {
    proxy: proxyPathsBuilder(6000, 4)
  }
};
