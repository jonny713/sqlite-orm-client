const api = {
  baseURL: document.location.pathname,
  async get(url) {
    const response = await fetch(url)

    if (!response.ok) {
      throw new Error("Failed to load data");
    }

    const data = await response.json();
    if (!Array.isArray(data)) {
      throw new Error("Invalid data");
    }

    return data;
  },
  async post(url, body) {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      body: JSON.stringify(body)
    });

    if (!response.ok) {
      throw new Error("Failed to post");
    }

    const result = await response.json();
    if (result.error) {
      throw new Error(result.error);
    }

    return result;
  },
  async delete(url) {
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      }
    })

    if (!response.ok) {
      throw new Error("Failed to delete");
    }

    const result = await response.json()
    if (result.error) {
      throw new Error(result.error)
    }

    return result
  },

  async getDBMetadata() {
    return await this.get(`${this.baseURL}/tables`)
  },
  async getDBTable(tableName) {
    return await this.get(`${this.baseURL}/tables/${tableName}`)
  },
  async addRecordToDB(tableName, record) {
    return await this.post(`${this.baseURL}/tables/${tableName}`, record)
  },
  async editRecordToDB(tableName, record) {
    return await this.post(`${this.baseURL}/tables/${tableName}/${record.id}`, record)
  },
  async deleteRecordFromDB(tableName, recordId) {
    return await this.delete(`${this.baseURL}/tables/${tableName}/${recordId}`)
  }
};
export default api;
