import Vue from "vue";
import VueRouter from "vue-router";
// import HomePage from "@/views/HomePage.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/:id",
    name: "TablesPage",
    component: () => import("@/views/AdminPage.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
